# read more

A Chrome extension that's designed to stop your persistent twitter/HN
refreshing by redirecting to your Kindle.

## Download

[Click here to download the extension](https://bitbucket.org/kevinburke/read-more/raw/master/read-more.crx)

#### Features

This is a minimum viable chrome extension, so there are no features. You get a
3-minute window to browse Tweetdeck, Twitter and Hacker News, otherwise you get
redirected to your Kindle. After an hour you can browse the site again.

