(function() {
    var bb = new BankersBox(1);

    var last_accessed_time = bb.get(window.location.hostname);
    var current_time = new Date().getTime();
    var beenToSiteBefore = last_accessed_time !== null;
    var recentVisit = current_time <= last_accessed_time + 1000 * 3  * 60;
    if (beenToSiteBefore &&
        current_time < last_accessed_time + 1000 * 60 * 60 && !recentVisit) {
        /* redirect to kindle */
        window.location.replace('https://read.amazon.com');
    } else if (beenToSiteBefore && recentVisit) {
        /* don't redirect, but don't update the value either */
    } else {
        /* set a new value */
        bb.set(window.location.hostname, current_time);
    }
})();
